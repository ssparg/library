<?php

namespace Tests\Feature;

use App\Models\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BookReservationTest extends TestCase
{
    use RefreshDatabase;

    public function testBookCanBeAddedToTheLibrary()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/books', [
            'title'     => 'My awesome book!',
            'author'    => 'Shaun Sparg'
        ]);

        $response->assertOk();

        $this->assertCount(1, Book::all());
    }

    public function testTitleIsRequired()
    {
        $response = $this->post('/books', [
            'title'     => '',
            'author'    => 'Shaun Sparg'
        ]);

        $response->assertSessionHasErrors('title');
    }

    public function testAuthorIsRequired()
    {
        $response = $this->post('/books', [
            'title'     => 'Cool Title',
            'author'    => ''
        ]);

        $response->assertSessionHasErrors('author');
    }

    public function testBookCanBeUpdated()
    {
        $this->withoutExceptionHandling();

        $this->post('/books', [
            'title'     => 'Cool Title',
            'author'    => 'Shaun Sparg'
        ]);

        $book = Book::first();

        $this->patch('/books/' . $book->id, [
            'title'     => 'New Title',
            'author'    => 'John Doe'
        ]);

        $this->assertEquals('New Title', Book::first()->title);
        $this->assertEquals('John Doe', Book::first()->author);
    }
}
