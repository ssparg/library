<?php

namespace App\Http\Controllers;

use App\Models\Book;

class BooksController extends Controller
{

    public function store()
    {
        Book::create($this->validateRequest());
    }

    /**
     * @param Book $book
     */
    public function update(Book $book)
    {
        $book->update($this->validateRequest());
    }

    /**
     * @return array
     */
    public function validateRequest(): array
    {
        return request()->validate([
            'title' => 'required',
            'author' => 'required',
        ]);
    }
}
